from PyQt5.QtCore import QSettings, Qt
from qgis.core import QgsCoordinateTransform, QgsFeature
from qgis.gui import QgsAttributeDialog, QgsAttributeEditorContext


def CRSTransform(mapCanvas, geom):
    layer = mapCanvas.currentLayer()
    renderer = mapCanvas.mapSettings()
    layerCRSSrsid = layer.crs().srsid()
    projectCRSSrsid = renderer.destinationCrs().srsid()

    # On the Fly reprojection.
    if layerCRSSrsid != projectCRSSrsid:
        geom.transform(QgsCoordinateTransform(projectCRSSrsid, layerCRSSrsid))


def createFeature(iface, mapCanvas, geom, editCommandTxt):
    if not geom.isEmpty():
        layer = mapCanvas.currentLayer()
        layer.beginEditCommand(editCommandTxt)
        settings = QSettings()

        CRSTransform(mapCanvas, geom)
        provider = layer.dataProvider()

        f = QgsFeature()
        f.setGeometry(geom)

        # add attribute fields to feature
        fields = layer.fields()

        # vector api change update
        f.initAttributes(fields.count())
        for i in range(fields.count()):
            f.setAttribute(i, provider.defaultValue(i))

        disable_attributes = settings.value(
            "/qgis/digitizing/disable_enter_attribute_values_dialog", False, type=bool
        )

        dlg = None

        dlg = iface.getFeatureForm(layer, f)
        ok = dlg.exec_()
        if ok:
            layer.changeGeometry(f.id(), geom)
            layer.endEditCommand()
        else:
            layer.destroyEditCommand()

        mapCanvas.refresh()
