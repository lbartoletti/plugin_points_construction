# -----------------------------------------------------------------
# Copyright (C) 2021 Oslandia
# Conctact : loic.bartoletti@oslandia.com ; contact@oslandia.com
# -----------------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# ---------------------------------------------------------------------

import os
import sys

from PyQt5 import uic
from PyQt5.QtCore import QSettings, Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QLineEdit, QToolButton, QWidget
from qgis.core import (
    QgsCircle,
    QgsFeature,
    QgsGeometry,
    QgsGeometryUtils,
    QgsPoint,
    QgsPointXY,
    QgsWkbTypes,
)
from qgis.gui import QgsAttributeDialog, QgsAttributeEditorContext, QgsRubberBand

from .qgssnappoint import QgsSnapPoint


class Intersection2Circles(QWidget):
    def __init__(self, iface, canvas, layer):
        super(Intersection2Circles, self).__init__()
        uic.loadUi(
            os.path.join(os.path.dirname(__file__), "intersection2circles.ui"), self
        )
        self.iface = iface
        self.canvas = canvas
        self.layer = layer

        self.pointCenter = QgsSnapPoint()
        self.mSelectCenter1.clicked.connect(lambda: self.select("circle1"))
        self.mSelectCenter2.clicked.connect(lambda: self.select("circle2"))
        self.mX1.valueChanged.connect(lambda: self.propertiesChanged("circle1"))
        self.mY1.valueChanged.connect(lambda: self.propertiesChanged("circle1"))
        self.mRadius1.valueChanged.connect(lambda: self.propertiesChanged("circle1"))
        self.mX2.valueChanged.connect(lambda: self.propertiesChanged("circle2"))
        self.mY2.valueChanged.connect(lambda: self.propertiesChanged("circle2"))
        self.mRadius2.valueChanged.connect(lambda: self.propertiesChanged("circle2"))
        self.mButtonBox.rejected.connect(self.deactivate)
        self.mButtonBox.accepted.connect(self.save)
        self.mIntersection1.toggled.connect(self.selectIntersectionPoint)
        self.mIntersection2.toggled.connect(self.selectIntersectionPoint)

        self.mX1.setRange(sys.float_info.min, sys.float_info.max)
        self.mY1.setRange(sys.float_info.min, sys.float_info.max)
        self.mX2.setRange(sys.float_info.min, sys.float_info.max)
        self.mY2.setRange(sys.float_info.min, sys.float_info.max)
        self.mRadius1.setMaximum(sys.float_info.max)
        self.mRadius2.setMaximum(sys.float_info.max)

        self.properties = {
            "circle1": {
                "x": self.mX1,
                "y": self.mY1,
                "radius": self.mRadius1,
                "center": QgsPoint(),
                "rubberBand": QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry),
            },
            "circle2": {
                "x": self.mX2,
                "y": self.mY2,
                "radius": self.mRadius2,
                "center": QgsPoint(),
                "rubberBand": QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry),
            },
        }

        self.properties["circle1"]["rubberBand"].setColor(QColor(255, 0, 0, 150))
        self.properties["circle1"]["rubberBand"].setWidth(2)
        self.properties["circle2"]["rubberBand"].setColor(QColor(255, 0, 0, 150))
        self.properties["circle2"]["rubberBand"].setWidth(2)

        self.intersection1rb = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.intersection1rb.setColor(QColor(0, 255, 0, 150))
        self.intersection1rb.setWidth(2)
        self.intersection1rb.setIcon(QgsRubberBand.ICON_X)
        self.intersection2rb = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.intersection2rb.setColor(QColor(0, 255, 0, 150))
        self.intersection2rb.setWidth(2)
        self.intersection2rb.setIcon(QgsRubberBand.ICON_X)

        self.intersection_selected = QgsRubberBand(
            self.canvas, QgsWkbTypes.PointGeometry
        )
        self.intersection_selected.setColor(QColor(0, 0, 255, 150))
        self.intersection_selected.setWidth(2)
        self.intersection_selected.setIcon(QgsRubberBand.ICON_BOX)

        self.intersection1 = QgsPoint()
        self.intersection2 = QgsPoint()
        self.intersection_point = QgsPoint()

        self.setWindowFlags(Qt.WindowStaysOnTopHint)

    def updateXY(self, center, circle):
        self.properties[circle]["x"].valueChanged.disconnect()
        self.properties[circle]["y"].valueChanged.disconnect()
        self.properties[circle]["center"] = center
        self.properties[circle]["x"].setValue(center.x())
        self.properties[circle]["y"].setValue(center.y())
        self.properties[circle]["x"].valueChanged.connect(
            lambda: self.propertiesChanged(circle)
        )
        self.properties[circle]["y"].valueChanged.connect(
            lambda: self.propertiesChanged(circle)
        )
        self.propertiesChanged(circle)

    def select(self, circle):
        try:
            self.pointCenter.disconnect()
        except Exception:
            pass

        self.canvas.setMapTool(self.pointCenter)

        self.pointCenter.point_signal.connect(
            lambda point: self.updateXY(point, circle)
        )

    def selectIntersectionPoint(self):
        if self.mIntersection1.isChecked():
            self.intersection_point = self.intersection1
        elif self.mIntersection2.isChecked():
            self.intersection_point = self.intersection2
        else:
            self.intersection_point = QgsPoint()
        self.updateIntersectionsRubberBand()

    def updateRubberBand(self, circle):
        circ = QgsCircle(
            self.properties[circle]["center"], self.properties[circle]["radius"].value()
        )
        self.properties[circle]["rubberBand"].setToGeometry(
            QgsGeometry(circ.toCircularString(True).clone()), self.layer
        )
        self.properties[circle]["rubberBand"].show()

    def propertiesChanged(self, circle):
        self.properties["circle1"]["center"].setX(self.mX1.value())
        self.properties["circle1"]["center"].setY(self.mY1.value())
        self.properties["circle2"]["center"].setX(self.mX2.value())
        self.properties["circle2"]["center"].setY(self.mY2.value())
        self.updateRubberBand(circle)
        nIntersections = self.calculateIntersections()
        self.selectIntersectionPoint()
        self.updateIntersectionsRadio(nIntersections)

    def calculateIntersections(self):
        center1xy = QgsPointXY(self.properties["circle1"]["center"])
        radius1 = self.properties["circle1"]["radius"].value()
        center2xy = QgsPointXY(self.properties["circle2"]["center"])
        radius2 = self.properties["circle2"]["radius"].value()
        nIntersections, inter1, inter2 = QgsGeometryUtils.circleCircleIntersections(
            center1xy, radius1, center2xy, radius2
        )
        self.intersection1 = QgsPoint(inter1)
        self.intersection2 = QgsPoint(inter2)
        return nIntersections

    def updateIntersectionsRubberBand(self):
        self.intersection1rb.setToGeometry(
            QgsGeometry(self.intersection1.clone()), self.layer
        )
        self.intersection2rb.setToGeometry(
            QgsGeometry(self.intersection2.clone()), self.layer
        )
        if not self.intersection_point.isEmpty():
            self.intersection_selected.setToGeometry(
                QgsGeometry(self.intersection_point.clone()), self.layer
            )
            self.intersection_selected.show()
        else:
            self.intersection_selected.hide()

    def updateIntersectionsRadio(self, intersection_number):
        if intersection_number == 0:
            self.mIntersection1.setEnabled(False)
            self.mIntersection2.setEnabled(False)
        if intersection_number == 1:
            self.mIntersection1.setEnabled(True)
            self.mIntersection2.setEnabled(False)
        if intersection_number == 2:
            self.mIntersection1.setEnabled(True)
            self.mIntersection2.setEnabled(True)
        self.updateIntersectionsRubberBand()

    def deactivate(self):
        self.properties["circle1"]["rubberBand"].hide()
        self.properties["circle2"]["rubberBand"].hide()
        self.intersection1rb.hide()
        self.intersection2rb.hide()
        self.intersection_selected.hide()
        self.close()

    def save(self):
        if not self.intersection_point.isEmpty():
            self.createFeature(QgsGeometry(self.intersection_point))
        self.deactivate()
        self.close()

    def createFeature(self, geom, showdlg=True):
        """
        create feature
        """
        layer = self.canvas.currentLayer()
        #        self.checkCRS()
        #        if self.layerCRS.srsid() != self.projectCRS.srsid():
        #            geom.transform(QgsCoordinateTransform(self.projectCRS, self.layerCRS, QgsProject.instance()))

        f = QgsFeature()
        fields = layer.fields()
        f.setFields(fields)
        f.setGeometry(geom)
        # add attribute fields to feature

        for i in range(fields.count()):
            f.setAttribute(i, feature.attributes()[i])

        settings = QSettings()
        disable_attributes = settings.value(
            "/qgis/digitizing/disable_enter_attribute_values_dialog", False, type=bool
        )
        if disable_attributes or showdlg is False:
            layer.beginEditCommand("Point construction intersection de 2 cercles")
            layer.addFeature(f)
            layer.endEditCommand()
        else:
            dlg = QgsAttributeDialog(layer, f, True)
            dlg.setAttribute(Qt.WA_DeleteOnClose)
            dlg.setMode(QgsAttributeEditorContext.AddFeatureMode)
            dlg.setEditCommandMessage("Point construction intersection de 2 cercles")
            ok = dlg.exec_()
