from PyQt5.QtCore import Qt, pyqtSignal
from qgis.core import QgsPoint
from qgis.gui import QgsMapToolCapture
from qgis.utils import iface


class QgsSnapPoint(QgsMapToolCapture):
    point_signal = pyqtSignal(QgsPoint)

    def __init__(self):
        super(QgsSnapPoint, self).__init__(
            iface.mapCanvas(), iface.cadDockWidget(), QgsMapToolCapture.CapturePoint
        )
        self.parentTool = iface.mapCanvas().mapTool()

    def cadCanvasPressEvent(self, e):
        if e.button() == Qt.LeftButton:
            self.point_signal.emit(self.mapPoint(e))
            self.deactivate()

        if e.button() == Qt.RightButton:
            self.deactivate()
