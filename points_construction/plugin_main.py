#! python3  # noqa: E265

"""
    Main plugin module.
"""

import os

# PyQGIS
from qgis.core import QgsApplication, QgsMapLayerType, QgsWkbTypes
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction
from qgis.utils import iface, showPluginHelp

# project
from points_construction.__about__ import __title__
from points_construction.gui.dlg_settings import PlgOptionsFactory
from points_construction.gui.intersection2circles import Intersection2Circles
from points_construction.toolbelt import PlgLogger, PlgTranslator

# ############################################################################
# ########## Classes ###############
# ##################################


class PointsConstructionPlugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.log = PlgLogger().log

        # translation
        plg_translation_mngr = PlgTranslator()
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr

    def initGui(self):
        """Set up plugin UI elements."""

        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        # -- Actions
        self.action_help = QAction(
            QIcon(":/images/themes/default/mActionHelpContents.svg"),
            self.tr("Help", context="PointsConstructionPlugin"),
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            lambda: showPluginHelp(filename="resources/help/index")
        )

        self.action_settings = QAction(
            QgsApplication.getThemeIcon("console/iconSettingsConsole.svg"),
            self.tr("Settings"),
            self.iface.mainWindow(),
        )
        self.action_settings.triggered.connect(
            lambda: self.iface.showOptionsDialog(
                currentPage="mOptionsPage{}".format(__title__)
            )
        )

        self.action_intersection2circles = QAction(
            QIcon(os.path.join(os.path.dirname(__file__), "icon.png")),
            "Intersection2Circles",
            self.iface.mainWindow(),
        )
        self.action_intersection2circles.triggered.connect(self.intersection2circles)

        # -- Menu
        self.iface.addPluginToMenu(__title__, self.action_settings)
        self.iface.addPluginToMenu(__title__, self.action_help)
        self.iface.addPluginToMenu(__title__, self.action_intersection2circles)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up menu
        self.iface.removePluginMenu(__title__, self.action_help)
        self.iface.removePluginMenu(__title__, self.action_settings)
        self.iface.removePluginMenu(__title__, self.action_intersection2circles)

        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)

        # remove actions
        del self.action_settings
        del self.action_help
        del self.action_intersection2circles

    def run(self):
        """Main process.

        :raises Exception: if there is no item in the feed
        """
        try:
            self.log(
                message=self.tr(
                    text="Everything ran OK.",
                    context="PointsConstructionPlugin",
                ),
                log_level=3,
                push=False,
            )
        except Exception as err:
            self.log(
                message=self.tr(
                    text="Houston, we've got a problem: {}".format(err),
                    context="PointsConstructionPlugin",
                ),
                log_level=2,
                push=True,
            )

    def intersection2circles(self):
        layer = iface.activeLayer()
        if (
            layer is not None
            and layer.type() == QgsMapLayerType.VectorLayer
            and layer.geometryType() == QgsWkbTypes.PointGeometry
        ):
            dlg = Intersection2Circles(iface, iface.mapCanvas(), layer)
            dlg.show()
